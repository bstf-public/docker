FROM docker:latest

RUN apk add --update --no-cache curl bash make gcc g++ build-base perl python3 python3-dev py3-pip openssl-dev libffi-dev libc-dev rust cargo nodejs git rsync gettext tar gzip grep npm

RUN pip3 --no-cache install --upgrade pip && \
    touch ~/.bash_profile && \
    pip3 install black==24.8.0 && \
    mkdir -p ~/.docker && \
    echo '{"experimental": "enabled"}' > ~/.docker/config.json && \
    pip3 install dump-env

RUN rm -Rf /usr/local/share/.cache /root/.cache /usr/local/bin/containerd /usr/local/bin/dockerd parallel*
ADD ./scripts/* /usr/sbin

